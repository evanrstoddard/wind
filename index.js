const express = require('express');
const fs = require('fs');
const app = express();

app.use(express.static('UI'));

app.get("/latest", (req, res) => {
    
    var dir = './files';

    fs.readdir(dir, function(err, files){
       
        files = files.map(function (fileName) {
            return {
                name: fileName,
                time: fs.statSync(dir + '/' + fileName).mtime.getTime()
            };
        });
        
        var logs = [];
        
        for (var i = 0; i < files.length; i++) {
            var filename = files[i].name;

            var ending = files[i].name.substr(files[i].name.length - 4, 4);
            if (ending == ".log") logs.push(files[i]);

        }

        logs = logs.sort(function (a, b) {
            return a.time - b.time; 
        }).map(function (v) {
            
            return v.name;
        });
        
        fs.readFile('./files/' + logs[logs.length-1], 'utf8', (err, raw) => {
            res.status(200).json({data:cleanFile(raw)});
        });
        
    });
    
});

app.get("/lastSevenDays", (req, res) => {
   
    var dir = './files';
    
    fs.readdir(dir, function(err, files) {
        files = files.map(function (fileName) {
            return {
                name: fileName,
                time: fs.statSync(dir + '/' + fileName).mtime.getTime()
            };
        });
        
        var logs = [];
        
        for (var i = 0; i < files.length; i++) {
            var filename = files[i].name;

            var ending = files[i].name.substr(files[i].name.length - 4, 4);
            if (ending == ".log") logs.push(files[i]);

        }

        logs = logs.sort(function (a, b) {
            return a.time - b.time; 
        }).map(function (v) {
            return v.name;
        });
        
        var data;
        
        if (logs.length < 8) {
            data = cleanFile(fs.readFileSync('./files/' + logs[0], 'utf8'));
            for (var i = 1; i < logs.length; i++) {
                var cleanedData = cleanFile(fs.readFileSync('./files/' + logs[i], 'utf8'));
                Object.keys(data).map(function(objectKey, index) {
                    data[objectKey] = data[objectKey].concat(cleanedData[objectKey]);
                }); 
            }    
        } else {
            data = cleanFile(fs.readFileSync('./files/' + logs[logs.length - 8], 'utf8'));
            for (var i = logs.length - 7; i < logs.length; i++) {
                var cleanedData = cleanFile(fs.readFileSync('./files/' + logs[i], 'utf8'));
                Object.keys(data).map(function(objectKey, index) {
                    data[objectKey] = data[objectKey].concat(cleanedData[objectKey]);
                }); 
            } 
        }
        
        res.status(200).json({data: data});

    });
    
});

function getFileData(path) {

  return new Promise(resolve => {
    
      fs.readFile(path, 'utf8', (err, raw) => {
            console.error(err);
            resolve(cleanFile(raw));
      });
      
  });
    
}

async function getFileDataSync(path) {
    
    var data = await getFileData(path);
    
    return data;
    
}

function cleanFile(raw) {
    
    var lines = raw.split("\r\n");
    
    var data = {};
    
    var keys = lines[5].split(";");
    
    for (var i = 0; i < keys.length - 1; i++) { data[keys[i]] = new Array();}
    
    for (var i = 9; i < lines.length - 1; i++) {
        
        var line = lines[i].split(";");

        for (var j = 0; j < keys.length - 1; j++) {
            
            if (j == 0) data[keys[j]].push(lines[2].split(": ")[1] + "  " + line[j]);
            else data[keys[j]].push(line[j]);
            
        }
        
    }

    return data;
}



app.listen(process.env.PORT || 5000)