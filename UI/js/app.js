class App {
    
    constructor() {

        this.showModal = this.showModal.bind(this);
        this.popView = this.popView.bind(this);
        this.pushView = this.pushView.bind(this);
        
        this.modal = null;
        this.views = [];
        this.tabBarController = new TabBarController();
        this.loadTabs = this.loadTabs.bind(this);
        
        
    }
    
    showModal(view) {
        
        var modalContainer = document.getElementById("modalContainer");
        modalContainer.classList.remove("hidden");
        view.show();
        
    }

    popView() {
      
        if (this.views.length) this.views.pop().hide();

    }
    
    pushView(view) {
        
        this.popView();
        view.show();
        this.views.push(view);
        
    }
    
    loadTabs() {
        var homeTab = new Tab(new HomeView(), "Home", "assets/icons8-home-page-40.png");
        
        this.tabBarController.addTabs([homeTab]);
    }
    
    
}
class TabBarController {
    
    constructor() {
        
        this.id = "tabBar";
        
        this.tabs = [];
        
        this.addTabs = this.addTabs.bind(this);
        
    }
    
    addTabs(tabs) {
        
        this.tabs = tabs;
        
        this.tabs.map((tab) => {
           
            document.getElementById(this.id).appendChild(tab.tabEl);
            
        });
        
        this.tabs[0].makeVisible();
        
    }
    
}
class Tab {
    
    constructor(view, title, image) {
        
        this.select = this.select.bind(this);
        this.deselect = this.deselect.bind(this);
        this.makeVisible = this.makeVisible.bind(this);
        
        this.view = view;
        this.title = title;
        this.image = image;
        this.tabEl = document.createElement("button");
        this.tabEl.classList.add("tabBar__item");
        
        var tabImageEl = document.createElement("img");
        tabImageEl.setAttribute("src", this.image);
        tabImageEl.classList.add("tabBar__item__image");
        this.tabEl.appendChild(tabImageEl);
        
        var tabTextEl = document.createElement("div");
        tabTextEl.classList.add("tabBar__item__text");
        tabTextEl.textContent = this.title;
        this.tabEl.appendChild(tabTextEl);
        
        this.tabEl.addEventListener("click", this.select, true);
        
    }
    
    select() {
        
        if (app.tabBarController.tabs.length < 2) return;
        
        this.makeVisible();
        
        
    }
    
    makeVisible() {
        app.tabBarController.tabs.map((tab) => {
            
            if (tab != this) tab.deselect();
            
        });
        
        if (!this.tabEl.classList.contains("tabBar__item--active")) {
            
            this.tabEl.classList.add("tabBar__item--active");
        
        } 
        document.getElementById("nav_title").textContent = this.title;
        app.pushView(this.view);
    }
    
    deselect() {
        
        this.tabEl.classList.remove("tabBar__item--active");
        app.popView(this.view);
        
    }
    
}
class View {
    
    constructor(app) {

        this.id = "view";
        
        this.visible = false;
        this.show = this.show.bind(this);
        
        this.viewWillAppear = this.viewWillAppear.bind(this);
        this.viewDidAppear = this.viewDidAppear.bind(this);
        this.viewWillDisappear = this.viewWillDisappear.bind(this);
        this.viewDidDisappear = this.viewDidDisappear.bind(this);
        
    }
    
    show() {
        
        this.visible = true;
        this.viewWillAppear();
        document.getElementById(this.id).classList.remove("hidden");
        this.viewDidAppear();
        
    }
    
    hide() {
        
        this.visible = false;
        this.viewWillDisappear();
        if (!document.getElementById(this.id).classList.contains("hidden")) document.getElementById(this.id).classList.add("hidden");
        this.viewDidDisappear();
        
    }
    
    viewWillAppear() {}
    viewDidAppear() {}
    viewWillDisappear() {}
    viewDidDisappear() {}
    
}

class HomeView extends View {
    
    constructor(app) {
        
        super(app);
        
        this.id = "view_home";
        
        this.powerGaugeEl = document.getElementById("powerGauge");
        this.powerGauge;
        this.powerValEl = document.getElementById("powerVal");
        
        this.inputVoltageGaugeEl = document.getElementById("inputVoltageGauge");
        this.inputVoltageGauge;
        this.inputVoltageValEl = document.getElementById("inputVoltageVal");
        
        this.outputVoltageEl = document.getElementById("outputVoltageGauge");
        this.outputVoltageGauge;
        this.outputVoltageValEl = document.getElementById("outputVoltageVal");
        
        this.powerVal = 0;
        this.inputVoltageVal = 0;
        this.outputVoltageVal = 0;
        
        this.interval;
        this.mapInterval;
        
        this.updatePowerGauge = this.updatePowerGauge.bind(this);
        this.updateInputVoltageGauge = this.updateInputVoltageGauge.bind(this);
        this.updateOutputVoltageGauge = this.updateOutputVoltageGauge.bind(this);
        
        this.updateView = this.updateView.bind(this);
        
        this.fetchMapData = this.fetchMapData.bind(this);
        this.updateMapData = this.updateMapData.bind(this);
        
        this.chartCtx = document.getElementById("prod_chart").getContext('2d');
        this.chart;
        
        this.chartCtx.width = $("#prod_chart").parent().width();
        this.chartCtx.height = $("#prod_chart").parent().height();
        
        this.data;
        
        this.chart = new Chart(this.chartCtx, {
            type: 'line',
            data: {
                labels: [],
                datasets: [{
                    label: 'Power',
                    data: [],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                },
                {
                    label: 'Input Voltage',
                    data: [],
                    backgroundColor: [
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                },
                {
                label: 'Output Voltage',
                    data: [],
                    backgroundColor: [
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            scaleOverride : true,
                            
                    }}],
                    xAxes: [
                        {ticks: {
                            callback: function(value) {
                                if (value.length > 4) {
                                    return value.substr(0, 4) + '...'; //truncate
                                  } else {
                                    return value
                                }
                            }
                        }}
                    ]
                },
                elements: { point: { radius: 0 } },
                tooltips: {
                    enabled: true,
                    mode: 'label',
                    callbacks: {
                      title: function(tooltipItems, data) {
                        var idx = tooltipItems[0].index;
                        return 'Title: ' + data.labels[idx]; //do something with title
                      }
                      
                    }
                  }
            }
        });
        
        window.onresize = () => {  
            
            this.chartCtx.width = $("#myChart").parent().width();
            this.chartCtx.height = $("#myChart").parent().height(); 
            this.chart.resize();
        }
        
    }
    
    fetchData() {
        
        fetch("/latest").then((res) => {
           
            res.json().then((raw) => {
                
                var data = raw.data;
                
                this.updatePowerGauge(data["PAC"][data["PAC"].length - 1]);
                this.updateInputVoltageGauge(data["VDC1"][data["VDC1"].length - 1]);
                this.updateOutputVoltageGauge(data["VAC"][data["VAC"].length - 1]);
            });
            
        });
        
    }
    
    updatePowerGauge(val) {
        
        this.powerVal = val;
        this.powerGauge.set(this.powerVal);
        this.powerValEl.innerHTML = val + " W";

    }
    
    updateInputVoltageGauge(val) {
        
        this.inputVoltageVal = val;
        this.inputVoltageGauge.set(this.efficiencyVal);
        this.inputVoltageValEl.innerHTML = val + " VDC";
    }
    
    updateOutputVoltageGauge(val) {
        
        this.outputVoltageVal = val;
        this.outputVoltageGauge.set(this.outputVoltageVal);
        this.outputVoltageValEl.innerHTML = val + " VAC";
    }
    
    viewWillAppear() {
        
        var opts = {
            angle: -0.2, // The span of the gauge arc
            lineWidth: 0.2, // The line thickness
            radiusScale: 1, // Relative radius
            pointer: {
                length: 0.6, // // Relative to gauge radius
                strokeWidth: 0.035, // The thickness
                color: '#000000' // Fill color
            },
            staticZones: [
                {strokeStyle: "#FFDD00", min: 80, max: 100}, // Yellow
                {strokeStyle: "#30B32D", min: 0, max: 80}, // Green
                {strokeStyle: "#F03E3E", min: 100, max: 200}  // Red
            ],
            limitMax: false,     // If false, max value increases automatically if value > maxValue
            limitMin: false,     // If true, the min value of the gauge will be fixed
            colorStart: '#6FADCF',   // Colors
            colorStop: '#8FC0DA',    // just experiment with them
            strokeColor: '#E0E0E0',  // to see which ones work best for you
            generateGradient: true,
            highDpiSupport: true,     // High resolution support
        }

        this.powerGauge = new Gauge(this.powerGaugeEl).setOptions(opts); // create sexy gauge!
        this.powerGauge.maxValue = 200; // set max gauge value
        this.powerGauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
        this.powerGauge.animationSpeed = 32; // set animation speed (32 is default value)
        this.powerGauge.set(this.powerVal); // set actual value
        
        this.inputVoltageGauge = new Gauge(this.inputVoltageGaugeEl).setOptions(opts); // create sexy gauge!
        this.inputVoltageGauge.maxValue = 200; // set max gauge value
        this.inputVoltageGauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
        this.inputVoltageGauge.animationSpeed = 32; // set animation speed (32 is default value)
        this.inputVoltageGauge.set(this.inputVoltageVal); // set actual value
        
        this.outputVoltageGauge = new Gauge(this.outputVoltageEl).setOptions(opts); // create sexy gauge!
        this.outputVoltageGauge.maxValue = 200; // set max gauge value
        this.outputVoltageGauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
        this.outputVoltageGauge.animationSpeed = 32; // set animation speed (32 is default value)
        this.outputVoltageGauge.set(this.outputVoltageVal); // set actual value
        
        this.updateView();
        this.updateMapData();
        
        
        this.interval = setInterval(() => this.updateView()
        ,1000);
        this.mapInterval = setInterval(() => this.updateMapData(), 1000);
        
    }
    
    fetchMapData() {
        
        fetch("/lastSevenDays").then((res) => {
           
            res.json().then((raw) => {
                
                var data = raw.data;
                
                this.chart.data.datasets[0].data = data["PAC"];
                this.chart.data.datasets[1].data = data["VDC1"];
                this.chart.data.datasets[2].data = data["VAC"];

                this.chart.data.labels = data["Time"];
                this.chart.update();
                

            });
            
        });
        
    }
    
    updateMapData() {
        
        this.fetchMapData();
        
    }
    
    updateView() {
        this.fetchData()
    }
    
    viewWillDisappear() {
        
        clearInterval(this.interval);
        
    }
    
}

class DetailView extends View {
    
    constructor(app) {
        
        super(app);
        
        this.id = "view_detail";
        
    }
    
}
class SystemView extends View {
    
    constructor(app) {
        
        super(app);
        
        this.id = "view_system";
        
    }
    
}
class MoreView extends View {
    
    constructor(app) {
        
        super(app);
        
        this.id = "view_more";
        
    }
    
}

var app;


document.addEventListener("DOMContentLoaded", function(event) {
    app = new App();
    app.loadTabs();
});
