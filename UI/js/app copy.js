//Add button listeners
function addListeners() {
    

}

//Tab Selector
function handleTabSelect(e) {
    
    //Serialize element
    var item = $(e);
    
    //Remove active attribute
    var tabBarItems = $(".tabBar__item");
    tabBarItems.removeClass("tabBar__item--active");
     
    //Make tapped item active
    item.addClass("tabBar__item--active");
    
    //Update nav title
    if (item.data("tab") == "home") $("#nav_title").html("Home");
    else if(item.data("tab") == "detail") $("#nav_title").html("Detailed Data");
    else if(item.data("tab") == "system") $("#nav_title").html("System");
    else $("#nav_title").html("Learn More");
    
    changeMainView(item.data("tab"));
    
}

//Handle swapping main view
function changeMainView(view) {
    
    $("#view_home").hide();
    $("#view_detail").hide();
    $("#view_system").hide();
    $("#view_more").hide();
    //Show selected view
    $("#view_" + view).show();
    
}

//View Objects
var view = {
    
    show: function(view) {
        $(view.id).show();
    },
    hide: function(view) {
        $(view.id).hide();
    }
    
}

view.home = {
    id: "#view_home",
    init: function() {
        
        var opts = {
            angle: -0.2, // The span of the gauge arc
            lineWidth: 0.2, // The line thickness
            radiusScale: 1, // Relative radius
            pointer: {
                length: 0.6, // // Relative to gauge radius
                strokeWidth: 0.035, // The thickness
                color: '#000000' // Fill color
            },
            staticZones: [
                {strokeStyle: "#FFDD00", min: 1500, max: 2249}, // Yellow
                {strokeStyle: "#30B32D", min: 0, max: 1499}, // Green
                {strokeStyle: "#F03E3E", min: 2250, max: 3000}  // Red
            ],
            limitMax: false,     // If false, max value increases automatically if value > maxValue
            limitMin: false,     // If true, the min value of the gauge will be fixed
            colorStart: '#6FADCF',   // Colors
            colorStop: '#8FC0DA',    // just experiment with them
            strokeColor: '#E0E0E0',  // to see which ones work best for you
            generateGradient: true,
            highDpiSupport: true,     // High resolution support
        }
        
        var target = document.getElementById("powerGauge");
        
        view.home.powerGauge = new Gauge(target).setOptions(opts); // create sexy gauge!
        view.home.powerGauge.maxValue = 3000; // set max gauge value
        view.home.powerGauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
        view.home.powerGauge.animationSpeed = 32; // set animation speed (32 is default value)
        view.home.powerGauge.set(view.home.gauges.powerVal); // set actual value
        
        var target = document.getElementById("efficiencyGauge");
        
        view.home.efficiencyGauge = new Gauge(target).setOptions(opts); // create sexy gauge!
        view.home.efficiencyGauge.maxValue = 3000; // set max gauge value
        view.home.efficiencyGauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
        view.home.efficiencyGauge.animationSpeed = 32; // set animation speed (32 is default value)
        view.home.efficiencyGauge.set(view.home.gauges.efficiencyVal); // set actual value
        
        var target = document.getElementById("windGauge");
        
        view.home.windGauge = new Gauge(target).setOptions(opts); // create sexy gauge!
        view.home.windGauge.maxValue = 3000; // set max gauge value
        view.home.windGauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
        view.home.windGauge.animationSpeed = 32; // set animation speed (32 is default value)
        view.home.windGauge.set(view.home.gauges.windVal); // set actual value
        
        var ctx = document.getElementById("prod_chart").getContext('2d');
        ctx.width = $("#prod_chart").parent().width();
        ctx.height = $("#prod_chart").parent().height(); 
        view.home.prod_chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ["Jan", "Feb", "Mar", "Apr", "May", "June"],
                datasets: [{
                    label: 'Power',
                    data: [10, 4, 3, 5, 9, 3,5,6,8,9,3,2,5,3,4,7,8,3,4],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            scaleOverride : true,
                            max:10
                        }
                    }]
                }
            }
        });
        
        window.onresize = function() {  
        
            ctx.width = $("#myChart").parent().width();
            ctx.height = $("#myChart").parent().height(); 
            view.home.prod_chart.resize();
        }
    },
    gauges: {
        powerVal: Math.random()*3000,
        efficiencyVal: Math.random()*3000,
        windVal: Math.random()*3000,
        update: function() {
            view.home.powerGauge.set(view.home.gauges.powerVal);
            view.home.efficiencyGauge.set(view.home.gauges.efficiencyVal);
            view.home.windGauge.set(view.home.gauges.windVal);
        }
    }
}

setInterval(function() {
    view.home.gauges.powerVal = Math.random()*3000;
    view.home.gauges.windVal = Math.random()*3000;
    view.home.gauges.efficiencyVal = Math.random()*3000;
    view.home.gauges.update();
//    view.home.prod_chart.data.datasets[0].data[0] = Math.random()*10;
//    view.home.prod_chart.data.datasets[0].data[1] = Math.random()*10;
//    view.home.prod_chart.data.datasets[0].data[2] = Math.random()*10;
//    view.home.prod_chart.data.datasets[0].data[3] = Math.random()*10;
//    view.home.prod_chart.data.datasets[0].data[4] = Math.random()*10;
//    view.home.prod_chart.data.datasets[0].data[5] = Math.random()*10;
//    view.home.prod_chart.update();
},1000);


